/**
 * Default div loader.
 */


/**
 * Switch to the given tab.
 *
 * @param target
 *    The id of the page object to show.
 * @param anchor
 *    If non-empty, a anchor to scroll to.
 */
function rb_do_switchto( target, anchor )
{
    var i;
    var objtargetpage;
    var objtargetbutton;

    if( undefined==anchor ) anchor = "";

    objtargetpage = $("#"+target);
    if( anchor == "" ) {
	    objtargetbutton = $("[data-rbpage='"+target+"']");
	} else {
	    objtargetbutton = $("[data-rbpage='"+target+"'][data-nlparagraph='"+anchor+"']");
	}

	// unselect all page buttons.
    $(".rb-page-button").parent().removeClass( "active" );
    // unselect all dropdown menues.
    $(".dropdown").removeClass( "active" );
    
    // Hide all content.
    $(".rb-content-fragment").hide();

    // Now activate the button.
    objtargetbutton.parent().addClass( "active" );
    // Also mark any parent dropdown menus active.
    objtargetbutton.parents(".dropdown").addClass( "active" );

    // Display the desired page ...
    objtargetpage.show();

    // ... and scroll, if desired.
    if( anchor != "" && anchor != "#" ) {

    	// document.location=document.location.toString().split('#')[0]+'#'+anchor;

    	// console.log( "switch to " + anchor );
        document.location.href = "#"+anchor;
    } else {
    	// console.log( "single page" );
    }

    return 0;
}

function rb_switchto( events ) {
	var target = $(this).attr( "data-rbpage" );
	var anchor = $(this).attr( "data-nlparagraph" );
	if( undefined==anchor ) anchor = "";
	rb_do_switchto( target, anchor );
	return 0;
}

$( document ).ready(function() {

	// Initialize page buttons
	$( ".rb-page-button" ).click( rb_switchto ).each( function( idx, el ) {
		var anchor = $(this).attr( "data-nlparagraph" );
		if( undefined==anchor || "#"==anchor ) anchor = "";
		$(this).attr( "href", "#"+anchor );
	});

    $( ".rb-gallery-links" ).click( function( event ) {
        event = event || window.event,
            target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    }).find( "img" ).css( "padding-bottom", "4px" );



	// Load start page. If we have a hash, try to use it.
    var pagehash = window.location.hash;
    console.log( "pagahash is "+pagehash );
    if( pagehash.startsWith( "#rbpage-" ) ) {
        console.log( "starts with magic" );
        var pagename = pagehash.substring( 8 );
        console.log( "magic is "+pagename );
        rb_do_switchto( "rb-content-"+pagename, "" );
    } else {
    	var objactive = $( ".rb-page-active" );
    	var objparent = objactive.parent();
    	rb_do_switchto( objactive.attr( "data-rbpage" ), objactive.attr( "data-nlparagraph" ) );    
    }
});
