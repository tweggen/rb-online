
(function( $ ) {
	console.log( "Creating rb.galleryUpload" );
	$.rb.galleryUpload = {
		id: "galleryUpload"
	};
	$.rb.galleryUpload.currentPage = 0;
})( jQuery );


/**
 * Return the URL to an image thumbnail.
 */
function getThumbnailUrl( filename )
{
	return RbBaseURL 
		+ "/image/get?n="
		+ encodeURIComponent( filename )
		+ "&s=t";
}


/**
 * representation.
 */
(function( $ ) { $.rb.galleryUpload.json2thumbhtml = function( val ) {
	var isEditor = RbLoggedIn;

	var htmlFeature = "";

	var display = 1;

	if( RbLoggedIn ) {
	} else {
	}

	// If the item is currently selected, we add another class.
	var isSelected = $.rb.galleryUpload.selectedFiles.hasOwnProperty( val.id );
	var strSelectedClass = "";
	if( isSelected ) {
		strSelectedClass = " rb-gallery-upload-thumb-holder-selected";
	}
	if( display ) {
		htmlFeature +=
			'<span '+
				'class="rb-gallery-upload-thumb-holder'+
					strSelectedClass+
					'" '+
				'data-rb-media-id="'+  val.id +'" '+
				'>'+
			'<div '+
				'class="rb-gallery-upload-thumb" '+
				'style="background-image:url(\''+getThumbnailUrl(val.path)+'\')\" '+
				'>'+
			'</div>'+
			'</span>';
	}
	console.log( htmlFeature );
	return htmlFeature;
};})( jQuery );


/**
 * Called, if a thumbnail image has been clicked.
 */
(function( $ ) { $.rb.galleryUpload.onClick = function( ev ) {
	ev.preventDefault();
	var divImage = ev.target.closest('.rb-gallery-upload-thumb-holder');
	// Read the image id.
	var id = $(divImage).attr( "data-rb-media-id" );
	if( !id || ""==id ) return;
	// Check, wether this image is selected
	if( $.rb.galleryUpload.selectedFiles.hasOwnProperty( id ) ) {
		// Then unselect
		delete $.rb.galleryUpload.selectedFiles[id];
		$(divImage).removeClass( "rb-gallery-upload-thumb-holder-selected" );		
	} else {
		$.rb.galleryUpload.selectedFiles[id] = {};
		$(divImage).addClass( "rb-gallery-upload-thumb-holder-selected" );				
	}
};})( jQuery );

/**
 * For a single thumbnail display, or for a list of all thumbnails, attach
 * handlers.
 */
(function( $ ) { $.rb.galleryUpload.applyThumbnailViewHandlers = function( context ) {
	// TXWTODO: I guess we need some kind of onclick. 
	if( false != context ) {
		$( '.rb-gallery-upload-thumb-holder', context ).click( $.rb.galleryUpload.onClick );
	} else {
		$( '.rb-gallery-upload-thumb-holder' ).click( $.rb.galleryUpload.onClick );
	}
};})( jQuery );


/**
 * Load a html version of all thumbnails to the given place. 
 */
(function( $ ) { $.rb.galleryUpload.loadThumbnailsTo = function( idTarget ) {
	$.getJSON( $.rb.rbDataUrl+"/media", function( data ) {

		var items = [];
		// TODO: We should use a dedicated editor rule;
		var isEditor = RbLoggedIn;

		// TXWTODO: Add this to a lambda passed to this method.
		if( 0==data.length ) {
			items.push( "<p>Kein Bild verf&uuml;gbar.</p>" );
		} else {
		  	$.each( data, function( key, val ) {
	  			var htmlThumbnail = $.rb.galleryUpload.json2thumbhtml( val );
		    	items.push( htmlThumbnail );
		  	});
		}	 
		$( '#'+idTarget ).empty();
	  	$( items.join( "" ) ).appendTo( "#" +idTarget );
	  	$.rb.galleryUpload.applyThumbnailViewHandlers( false );
	});
};})( jQuery );


(function ( $ ) {

 	// Change this to the location of your server-side upload handler:
    var url = RbBaseURL + "php/upload/"; 
    $('#rbGalleryUploadFileUpload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
        	/*
             * add the image to the database, reload the load page.
             * TXWTODO: If adding to the db fails, we do not clean
             * up the file list.
             */

			//var deleteUrls = [];
			var arrFiles = [];
			$.each(data.originalFiles, function( index, file ) {
				arrFiles.push({
					'path': file.name,
					'mimetype': file.type
				});
				// deleteUrls.push({
				//	'url': file.deleteUrl,
				//	'httptype': file.deleteType 
				//});
			});
			var arrPostData = {
				'author': 1, // TXWTODO: How to obtain user id?
				'creationdate': (Math.floor((new Date).getTime()/1000)),
				'publishdate': (Math.floor((new Date).getTime()/1000)),
				'files': arrFiles
			};
			// TXWTODO: Append an spinner as upload.
			$.ajax({
				type: 'POST',
				url: $.rb.rbDataUrl+"/media",
				data: arrPostData,
				success: function( data ) {
					$('#rbGalleryUploadFileUploadProgress .progress-bar').css(
            		    'width', '0%' );
 					// $.rb.gallery.closeEdit();
					console.log( "Created:" );
					console.log( data );
					$.rb.galleryUpload.loadPage( 1 );
				},
				dataType: 'json',
				error: function() {
					$('#rbGalleryUploadFileUploadProgress .progress-bar').css(
            		    'width', '0%' );
 					alert( "Error adding!" );
					// TXWTODO: Remove the file from the file server.
				},
			});

            if(0) {
	            $.each(data.result.files, function (index, file) {
	                $('<p/>').text(file.name).appendTo('#rbGalleryUploadFileUploadFiles');
	            });
	            // Fallback
	            $('#rbGalleryUploadFileUploadHiddenName').val(data.result.files[0].name);
	            // Append to file list
	 		}          
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#rbGalleryUploadFileUploadProgress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	// Attach the proper event handlers.
	$( '#rbGalleryUploadModalForm' ).submit( function( ev ) {
		// Do not reload page.
		ev.preventDefault();

		// Create selected images string and pass to caller.
		var strMediaIds = "";
		var isFirst = true;
		$.each( $.rb.galleryUpload.selectedFiles, function( id, val ) {
			if( isFirst ) {
				strMediaIds += ""+id;
				isFirst = false;
			} else {
				strMediaIds += ","+id;
			}
		});
		$( "#rbGalleryUploadModal" ).modal( "hide" );

		if( $.rb.galleryUpload.onSubmitIds( strMediaIds ) );

	});
})( jQuery );

(function( $ ) { $.rb.galleryUpload.loadPage = function() {
	$.rb.galleryUpload.loadThumbnailsTo( "rbGalleryUploadModalGrid" );
};})( jQuery );


/**
 * Open the gallery upload dialog, passing the ids of the selected files.
 */
(function( $ ) { $.rb.galleryUpload.open = function( arrSelectedFiles, onSubmitIds )
{
	//$( ".rb-gallery-upload-thumb-holder-selected
	var jqGrid = $( '#rbGalleryUploadModalGrid' );
	jqGrid.empty();	
	$.rb.galleryUpload.onSubmitIds = onSubmitIds;
	$.rb.galleryUpload.selectedFiles = {};
	if( Array.isArray( arrSelectedFiles ) ) {
		$.each( arrSelectedFiles, function( idx, val ) {
			$.rb.galleryUpload.selectedFiles[val] = {};
		});
	}
	$( "#rbGalleryUploadModal" ).modal( "show" );
	$.rb.galleryUpload.loadPage( 1 );
};})( jQuery );
