
function jsFriendlyJSONStringify (s) {
    return JSON.stringify(s).
        replace(/\u2028/g, '\\u2028').
        replace(/\u2029/g, '\\u2029');
}

(function( $ ) {
	console.log( "Creating rb.features" );
	$.rb.features = { 
		id: "features",
		// This is called before something new is edited.
		closeEditContext: false,
		currentDeleteId: -1,
	};	

	// Append confirm delete box.
	$( 'body' ).append( 
		'<div id="rbDeleteFeatureConfirm" class="modal fade" tabindex="-1" role="dialog">'
			+'<div class="modal-dialog">'
				+'<div class="modal-content">'
					+'<div class="modal-header">'
						+'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
						+'<h4 class="modal-title">'
							+'Artikel l&ouml;schen'
						+'</h4>'
					+'</div>'
					+'<div class="modal-body">'
						+'<p>M&ouml;chtest Du den Artikel wirklich unwiderruflich l&ouml;schen?</p>'
					+'</div>'
					+'<div class="modal-footer">'
						+'<button type="button" id="rbDeleteFeatureConfirmDismiss" class="btn btn-default" data-dismiss="modal">Nein</button>'
						+'<button type="button" id="rbDeleteFeatureConfirmSubmit" class="btn btn-primary">Artikel l&ouml;schen</button>'
					+'</div>'
				+'</div>'
			+'</div>'
		+'</div>' );

})( jQuery );


/**
 * Return the URL to an image thumbnail.
 */
function getThumbnailUrl( filename )
{
	return RbBaseURL 
		+ "/image/get?n="
		+ encodeURIComponent( filename )
		+ "&s=t";
}


/**
 * Return the URL to an image thumbnail.
 */
function getTimelineUrl( filename )
{
	return RbBaseURL 
		+ "/image/get?n="
		+ encodeURIComponent( filename )
		+ "&s=n";
}


var entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
	};


/**
 * we convert anything to text until we encounter #&#, then we convert verbatim
 * until the next #&#.
 */
function escapeHtml(str) {
	var dest = "";
	var curridx = 0;
	var mode = 0; // if 0, quote string, if 1, copy verbatim.
	while(true) {
		var nextidx = str.indexOf("#&#", curridx);
		var copynow;
		if(-1==nextidx) {
			copynow = str.length-curridx;
		} else {
			copynow = nextidx-curridx;
		}
		var part = str.substr(curridx, copynow);
		if(0==mode) {
			dest += String(part).replace(/[&<>"'\/]/g, function (s) {
  				return entityMap[s];
			});
			mode = 1;
		} else {
			dest += part;
			mode = 0;
		}
		if(-1==nextidx) {
			break;
		}
		curridx  = nextidx+3;
	}
	return dest;
}


function timeConverter( epochtime ){
	var a = new Date( epochtime * 1000 );
	//var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	//var month = months[a.getMonth()];
	var month = a.getMonth()+1;
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + '.' + month + '.' + year + ' ' + hour + ':' + min + ':' + sec ;
	return time;
}


/**
 * Take a json feature object and apply it to a form.
 */
(function( $ ) { $.rb.features.json2form = function( context, data ) {
	$( 'input[name="rbPostFeatureTitle"]', context )
		.val( data.title );
	$( 'textarea[name="rbPostFeatureContent"]', context )
		.val( data.newsdata );
	var val="";
	switch( data.visibility ) {
	case "0": val="hidden"; break;
	case "1": val="editors"; break;
	case "2": val="members"; break;
	case "3": val="public"; break;
	default: val="hidden"; break;
	}
	$( 'select[name="rbPostFeatureVisibility"]', context )
		.val( val );
};})( jQuery );


/**
 * Take a json representation of a feature and create the html
 * representation.
 */
(function( $ ) { $.rb.features.json2html = function( val ) {
	var isEditor = RbLoggedIn;

	var htmlFeature = "";

	var display = 1;

	if( RbLoggedIn ) {
		switch( val.visibility ) {
		case "0": display = 1; break;
		case "1": display = 1; break;
		case "2": display = 1; break;
		case "3": display = 1; break;
		default: console.log( "error1" ); break;
		}
	} else {
		switch( val.visibility ) {
		case "0": display = 0; break;
		case "1": display = 0; break;
		case "2": display = 0; break;
		case "3": display = 1; break;
		default: console.log( "error1" ); break;
		}
	}

	if( display ) {
		// Compute the string containing the current image ids.
		var isFirst;
		var strImageIds = "";
		var jImages = {};
		isFirst = true;
		$.each( val.media, function( idx, el ) {
			jImages[el.id] = el.path;
			if( isFirst ) {
				strImageIds += ""+el.id;
				isFirst = false;
			} else {
				strImageIds += ","+el.id;
			}
		});
		var strImageJson = encodeURI( jsFriendlyJSONStringify( jImages ) );

		/*
		 * Now render, depending on visibility, publishing date and rendertype.
		 */
		switch( val.rendertype ) {
		case "0":
			var content = escapeHtml( val.newsdata );
			content = content.replace( /\n\n/gm, "</p><p>");
			// Plain text.
			// console.log( val );
			htmlFeature = 
				 "<div class='rb-mainpanel-inner'>"
				+"<div class='rb-panel rb-feature-or-edit'>"
				+"<div class='rb-feature-div'"
					+" data-rb-feature-id='"+ val.id+"' "
					+" id='feature-"+val.id+"'>"
				+"<div class='rb-feature-head'>"
				+"<h4 class='rb-feature-title'>"
				+ escapeHtml( val.title )
				+"<span class='pull-right'>"
				;
			if( isEditor ) {
	    		//htmlFeature += ""
	    			/* +"<div class='dropdown'>"
	    			+"<span class='glyphicon glyphicon-eye-open dropdown-toggle'"
	    				+" aria-hidden='true' data-toggle='dropdown'"
	    				+" aria-haspopup='true' aria-expanded='true'>"
	    			+"</span>&nbsp;"
	    			+"<ul>"
	    				+"<li><a href='#'>Versteckt</a>"
	    				+"<li><a href='#'>Redakteure</a>"
	    				+"<li><a href='#'>Mitglieder</a>"
	    				+"<li><a href='#'>&Ouml;ffentlich</a>"
	    			+"</ul>"
	    			+"</div>" */
	    			/*
	    			+"<span data-toggle='tooltip'"
	    				+" title='Sichtbarkeit einstellen'"
	    				+" class='glyphicon glyphicon-eye-open"
	    					+" rb-button rb-button-feature-visibility'"
	    				+" aria-hidden='true'></span>&nbsp;"
	    			;*/
	    		if( "0" != val.visibility ) {
		    		htmlFeature += ""
		    			+"<span data-toggle='tooltip'"
		    				+" title='Artikel löschen'"
		    				+" class='glyphicon glyphicon-trash"
		    					+" rb-button rb-button-feature-delete'"
		    				+" aria-hidden='true'></span>&nbsp;"
		    			;
    			} else {
		    		htmlFeature += ""
		    			+"<span data-toggle='tooltip'"
		    				+" title='Artikel wiederherstellen'"
		    				+" class='glyphicon glyphicon-repeat"
		    					+" rb-button rb-button-feature-recycle'"
		    				+" aria-hidden='true'></span>&nbsp;"
		    			;
       			}
	    		htmlFeature += ""
	    			+"<span data-toggle='tooltip'"
	    				+" title='Artikel bearbeiten'"
	    				+" class='glyphicon glyphicon-pencil"
	    					+" rb-button-feature-edit'"
    					+" aria-hidden='true'></span>"
	    			;
	    	}
	    	htmlFeature += ""
				+"</span>"
				+"</h4>"
				+"<small>"
				+timeConverter( val.creationdate )
				+" von "
				+val.author
				+", ";
			switch( val.visibility ) {
			case "0": htmlFeature += "gel&ouml;scht"; break;
			case "1": htmlFeature += "nur f&uuml;r Redakteure"; break;
			case "2": htmlFeature += "nur f&uuml;r Mitglieder"; break;
			case "3": htmlFeature += "&ouml;ffentlich"; break;
			default: htmlFeature += "komisch"; break;
			}
			htmlFeature += ""
				+"</small>"
				+"</div>" // rb-feature-head
				;
			if( "0" != val.visibility ) {
				// Any media to render before article.
				htmlFeature += 
					"<div "+
						"class='rb-feature-media-description' "+
						"data-rb-feature-media-ids='"+ strImageIds + "' "+
						"data-rb-feature-media-obj='"+ strImageJson + "' "+
						"></div>"; // feature-media-description
				if( val.media && val.media.length ) {
					// We render the first image full-width
					isFirst = true;
					isFirstThumb = true;
					var wasOpen = false;
					$.each( val.media, function( idx, el ) {
						if( isFirst ) {
							htmlFeature += 
								"<div class='rb-feature-main-image-holder'>"+
								//"<div class='rb-feature-main-image' "+
								//	'style="background-image:url(\''+RbFilesRoot+el.path+'\')\" '+
								//	"class='rb-feature-main-image'>"+
								"<img class='rb-feature-main-image' "+
									'src="'+getTimelineUrl(el.path)+'" '+
									'>'+
								"</div>";
							isFirst = false;
						} else {
							if( !wasOpen ) {
								wasOpen = true;
								htmlFeature += "<div class='rb-feature-some-image-allholder'>";
							}
							if( !isFirstThumb ) {
								htmlFeature += " ";
							} else {
								isFirstThumb = false;
							}
							htmlFeature += 
								"<div class='rb-feature-some-image-holder'>"+
								"<span "+
									'style="background-image:url(\''+getThumbnailUrl(el.path)+'\')\" '+
									'data-rb-feature-media-id="'+el.id+'" '+
									"class='rb-feature-some-image'>"+
								"</span>"+
								"</div>";
						}
					});
					if( wasOpen ) {
						// Close other images holder.
						htmlFeature +=
							"<span class='rb-feature-some-image-allholder-stretch'></span>"+
							"</div>"; // some-image-allholder
					}
				}
				htmlFeature += ""
					+"<p class='rb-feature-text'>"+content+"</p>"
					;
			}
			htmlFeature += ""
				+"</div>" // rb-feature-div
				+"</div>" // rb-panel
				+"</div>" // rb-mainpanel-inner
				;
			// console.log( htmlFeature );
			break;
		default:
			console.log( "Unknown rendertype" );
		}
	}
	return htmlFeature;
};})( jQuery );


/**
 * Extract the input data from the post/edit feature form and convert
 * it to an array.
 */
(function( $ ) { $.rb.features.form2array = function( form ) {
	// TODO: Validate
	var valVisibility = "0";
	var strVisibility = $('#rbPostFeatureForm').find('select[name="rbPostFeatureVisibility"]').val();
	switch( strVisibility ) {
		case 'hidden': valVisibility = "0"; break;
		case 'editors': valVisibility = "1"; break;
		case 'members': valVisibility = "2"; break;
		case 'public': valVisibility = "3"; break;
		default: valVisibility = "0"; break;
	}
	var form = $('#rbPostFeatureForm');
	var strMediaIds = $( 'input[name="rbPostFeatureMediaIds"]', form ).val();

	var arrPostData = {
		'title': form.find('input[name="rbPostFeatureTitle"]').val(),
		'author': 1, // TXWTODO: How to obtain user id?
		'creationdate': (Math.floor((new Date).getTime()/1000)),
		'publishdate': (Math.floor((new Date).getTime()/1000)),
		'rendertype': 0,
		'visibility': valVisibility,
		'newsdata': form.find('textarea[name="rbPostFeatureContent"]').val(),
		'media': ("["+strMediaIds+"]")
	};
	return arrPostData;
};})( jQuery );


(function( $ ) { $.rb.features.closeEdit = function() {
	var closeEditContext = $.rb.features.closeEditContext;
	if( closeEditContext ) {
		$.rb.features.closeEditContext = false;
		closeEditContext();	
	}
};})( jQuery ) ;


/**
 * Open the update box.
 */
(function( $ ) { $.rb.features.openUpdateDialog = function( divArticle ) {
	$.rb.features.closeEdit();

	var divPanel = divArticle.closest( '.rb-feature-or-edit' );
	$( divArticle ).hide();

	// Remember, that if we are closed, the article needs to be 
	// shown again.
	$.rb.features.closeEditContext = function() {
		$( '#rbPostFeatureForm' )
			.unbind( "submit" )
			.unbind( "reset" )
			; 
		// Put the editor back to somewhere else.
		$( '#rbPostFeatureDiv' )
			.hide()
			.detach().appendTo( '#rbPostFeatureParent');
		$( divArticle ).show();
	};

	var divEditor = $( '#rbPostFeatureDiv' ).detach();

	// Setup empty content and trigger loading the single article.
	$( 'input[name="rbPostFeatureTitle"]', divEditor )
		.val( "(Loading)" );
	$( 'textarea[name="rbPostFeatureContent"]', divEditor )
		.val( "(Loading)" );
	$( 'button', divEditor ).prop( "disabled", true );
	$( '.rb-feature-input', divEditor ).prop( "disabled", true );
	$( '#rbPostEditTitle', divEditor ).text( "Artikel bearbeiten" );
	$( '#rbPostFeaturePublish', divEditor ).text( "Speichern" );
	// Read media ids.
	var strMediaIds= $( '.rb-feature-media-description', divArticle )
		.attr( "data-rb-feature-media-ids" );
	$( '.rb-post-feature-media-ids', divEditor ).val( strMediaIds );
	divEditor.appendTo( divPanel );

	divEditor.show();

}; })( jQuery );


/**
 * Called when the edit button is pressed on an feature.
 */
(function( $ ) { $.rb.features.onUpdate = function( ev ) {
	var divArticle = ev.target.closest( '.rb-feature-div' );
	$.rb.features.currentUpdateDiv = $( divArticle ).parent().parent();

	$.rb.features.openUpdateDialog( divArticle );

	var id = $( divArticle ).attr( "data-rb-feature-id" );

	// Attach the proper event handlers.
	$( '#rbPostFeatureForm').submit( function( ev ) {
		ev.preventDefault();

		var arrPutData = $.rb.features.form2array( '#rbPostFeatureForm' );
		console.log( "Triggering PUT." );
		$.ajax({
			type: 'PUT',
			url: $.rb.rbDataUrl+"/feature/"+ id,
			data: arrPutData,
			success: function( data ) {
				$.rb.features.closeEdit();
				var newArticle = $.parseHTML( $.rb.features.json2html( data ) );
				$.rb.features.applyFeatureViewHandlers( newArticle );
				$( $.rb.features.currentUpdateDiv ).replaceWith( newArticle );
				$.rb.features.currentUpdateDiv = false;
				// Update news.
				$.rb.features.reloadNews();
			},
			dataType: 'json',
			error: function() {
				alert( "Error putting!" );
			},
		});
	});

	$( '#rbPostFeatureForm' ).bind( 'reset', function( ev ) { 
		ev.preventDefault();
		$.rb.features.closeEdit();
	});


	// And load the article into the empty editor.
	$.getJSON( 
		$.rb.rbDataUrl 
			+ "/feature/"
			+ $( divArticle ).attr( "data-rb-feature-id" ), 
		function( data ) {

			var divEditor = $( '#rbPostFeatureDiv' );
			$.rb.features.json2form( divEditor, data );

			$( 'button', divEditor ).prop( "disabled", false );
			$( '.rb-feature-input', divEditor ).prop( "disabled", false );
			// $( divEditor ).show();
			$( '#rbPostFeatureTitle', divEditor ).focus();
	});
};})( jQuery );


/**
 * Called, if the user presses delete in the confirm delete modal.
 * This expects $.rb.features.currentDeleteId to contain the id
 * of the article to delete.
 */
(function( $ ) { $.rb.features.onDeleteConfirm = function( ev ) {
	$( "#rbDeleteFeatureConfirm" ).modal( "hide" );

	console.log( "Triggering DELETE." );
	id = $.rb.features.currentDeleteId;

	if( -1==id ) {
		console.log( "Internal error: No id for delete confirm modal." );
		return;
	}
	$.ajax({
		type: 'DELETE',
		url: $.rb.rbDataUrl+"/feature/"+ id,
		// data: { id: id },
		success: function( data ) {
			console.log( data.oldfeature );
			var newArticle = $.parseHTML( $.rb.features.json2html( data.oldfeature ) );
			$.rb.features.applyFeatureViewHandlers( newArticle );
			$( $.rb.features.currentDeleteDiv ).replaceWith( newArticle );
			$.rb.features.currentDeleteDiv = false;
		},
		dataType: 'json',
		error: function() {
			alert( "Error deleting!" );
		},
	});

};})( jQuery );


(function( $ ) { $.rb.features.onDelete = function( ev ) {
	// Deletes terminates any editing.
	$.rb.features.closeEdit();

	var divArticle = ev.target.closest( '.rb-feature-div' );
	var id = $( divArticle ).attr( "data-rb-feature-id" );
	$.rb.features.currentDeleteId = id;
	$.rb.features.currentDeleteDiv = $( divArticle ).parent().parent();

	$( "#rbDeleteFeatureConfirm" ).modal( "show" );

};})( jQuery );


/**
 * A feature's image has been clicked on. Open the image gallery with
 * all images belonging to this article.
 */
(function( $ ) { $.rb.features.onImageClicked = function( ev, divArticle, id ) {
	// read all media ids belonging to this article.
	var strMediaObj = $( '.rb-feature-media-description', divArticle ).attr( 'data-rb-feature-media-obj' );
	console.log( strMediaObj );
	var jMediaObj = JSON.parse( decodeURI( strMediaObj ) );
	// Now create images in the "links" list for the blueimp gallery.
	var divLinks = $( '#links' );
	divLinks.empty();
	var html = "";
	$.each( jMediaObj, function( idx, val ) {
		html +=
			'<a href="' + getTimelineUrl( val ) + '">'+
				'<img src="' + getThumbnailUrl( val ) + '">'+
			'</a>';
	});
	$( html ).appendTo( "#links" );
	var linklist = document.getElementById( 'links' ).getElementsByTagName( "a" );
	blueimp.Gallery( linklist, { index: ev.target.src, event: ev } );
};})( jQuery );

/**
 * Click on thumbnail: Open thumb gallery.
 */
(function( $ ) { $.rb.features.onThumbClicked = function( ev ) {
	var divArticle = ev.target.closest( '.rb-feature-div' );
	$.rb.features.currentUpdateDiv = $( divArticle ).parent().parent();
	var strMediaId = $( ev.target ).attr( "data-rb-feature-media-id" );
	$.rb.features.onImageClicked( ev, divArticle, strMediaId );
};})( jQuery );


/**
 * For a single feature display, or for a list of all features, attach
 * the edit/discard etc. handlers.
 */
(function( $ ) { $.rb.features.applyFeatureViewHandlers = function( context ) {
	if( false != context ) {
 	 	$( '.rb-button-feature-edit', context ).click( $.rb.features.onUpdate );
 	 	$( '.rb-button-feature-delete', context ).click( $.rb.features.onDelete );
 	 	$( '.rb-feature-some-image', context ).click( $.rb.features.onThumbClicked );
 	 	// $( '.rb-feature-main-image', context ).click( $.rb.features.onMainImageClicked );
 	} else {
 	 	$( '.rb-button-feature-edit' ).click( $.rb.features.onUpdate );
 	 	$( '.rb-button-feature-delete' ).click( $.rb.features.onDelete );
 	 	$( '.rb-feature-some-image' ).click( $.rb.features.onThumbClicked );
 	 	// $( '.rb-feature-main-image' ).click( $.rb.features.onMainImageClicked );
  	}
};})( jQuery );


/**
 * Load a html version of all features to the given place. 
 */
(function( $ ) { $.rb.loadFeaturesTo = function( idTarget ) {
	$.getJSON( $.rb.rbDataUrl+"/feature", function( data ) {

		var items = [];
		// TODO: We should use a dedicated editor rule;
		var isEditor = RbLoggedIn;

	  	$.each( data, function( key, val ) {
  			var htmlFeature = $.rb.features.json2html( val );
	    	items.push( htmlFeature );
	  	});
	 
	  	$( items.join( "" ) ).appendTo( "#" +idTarget );
	  	$.rb.features.applyFeatureViewHandlers( false );
	});
};})( jQuery );


/**
 * Helper function for textareas: workaround unexpected cr key presses. 
 */
$( ".rb-no-enter-submit" ).bind("keypress", function (e) {
    if( e.keyCode == 13 ) {
        return false;
    }
});


/**
 * We are currently editing an article, and the add images has been called.
 */
(function( $ ) { $.rb.features.onSubmitEditMediaIds = function( strMediaIds ) {
	var divEditor = $( '#rbPostFeatureDiv' );
	// And write back the new media ids.
	$( '.rb-post-feature-media-ids', divEditor ).val( strMediaIds );
};})( jQuery );


/**
 * Open the post article display.
 */
(function( $ ) { $.rb.features.openPost = function() {
	$.rb.features.closeEdit();

	$.rb.features.closeEditContext = function() {
		// Put the editor back to somewhere else.
		$( '#rbPostFeatureForm' )
			.unbind( "submit" )
			.unbind( "reset" )
			; 
		$( '#rbPostFeatureDiv' )
			.hide()
			.detach()
			.appendTo( '#rbPostFeatureParent' )
			;
	};

	var divEditor = $( '#rbPostFeatureDiv' ).detach();

	// Setup empty content and trigger loading the single article.
	$( 'input[name="rbPostFeatureTitle"]', divEditor )
		.val( "" );
	$( 'textarea[name="rbPostFeatureContent"]', divEditor )
		.val( "" );
	$( 'input[name="rbPostFeatureMediaIds"]', divEditor )
		.val( "" );

	$( 'button', divEditor ).prop( "disabled", false );
	$( '.rb-feature-input', divEditor ).prop( "disabled", false );
	$( '#rbPostEditTitle', divEditor ).text( "Neuen Artikel schreiben:" );
	$( '#rbPostFeaturePublish', divEditor ).text( "Veröffentlichen" );

	divEditor.appendTo( '#rbPostFeatureParent');

	// Attach the proper event handlers.
	$( '#rbPostFeatureForm' ).submit( function( ev ) {
		// Do not reload page.
		ev.preventDefault();

		var arrPostData = $.rb.features.form2array( '#rbPostFeatureForm' );
		$.ajax({
			type: 'POST',
			url: $.rb.rbDataUrl+"/feature",
			data: arrPostData,
			success: function( data ) {
				$.rb.features.closeEdit();
				var htmlFeature = $.rb.features.json2html( data );
				var newArticle = $( "#rbFeaturesHeadDummy" ).prepend( htmlFeature );
				$.rb.features.applyFeatureViewHandlers( newArticle );
				$.rb.features.reloadNews();
			},
			dataType: 'json',
			error: function() {
				alert( "Error posting!" );
				// Stay in post.
			},
		});
	});

	$( '#rbPostFeatureDiscard' ).click( function( ev ) {
		$( '#rbPostFeatureDiv' ).collapse( 'hide' );
	});

	$( '#rbPostFeatureDiv' ).show();
	$( '#rbPostFeatureDiv' ).collapse( 'show' );
	$( '#rbPostFeatureTitle' ).focus();

};})( jQuery );


/** 
 * Feature module global button "openPost"
 */
$( '#rbShowPostFeature' ).click( function( ev ) {
	$.rb.features.openPost();
});


/**
 * (Re)-load headlines for news container.
 */
(function( $ ) { $.rb.features.reloadNews = function() {
	var news = $( '#rbFeatureHeadlineContainer' );
	news.empty();
	$.getJSON( $.rb.rbDataUrl+"/feature", function( data ) {

		var items = [];
		// TODO: We should use a dedicated editor rule;
		var isEditor = RbLoggedIn;

	  	$.each( data, function( key, val ) {
			if( RbLoggedIn ) {
				switch( val.visibility ) {
				case "0": display = 1; break;
				case "1": display = 1; break;
				case "2": display = 1; break;
				case "3": display = 1; break;
				default: console.log( "error1" ); break;
				}
			} else {
				switch( val.visibility ) {
				case "0": display = 0; break;
				case "1": display = 0; break;
				case "2": display = 0; break;
				case "3": display = 1; break;
				default: console.log( "error1" ); break;
				}
			}

			if( display ) {
	  			var htmlFeature = "<p><a href='#feature-"
  					+val.id
  					+"'>"+escapeHtml( val.title )+"</a></p>";
	    		items.push( htmlFeature );
	    	}
	  	});
	 
	  	$( items.join( "" ) ).appendTo( '#rbFeatureHeadlineContainer' );
	});
};})( jQuery );


$( document ).ready( function() {
	$.rb.loadFeaturesTo( "rbFeaturesHeadDummy" );
	$.rb.features.reloadNews();
	$( '#rbDeleteFeatureConfirmSubmit' ).click( function( ev ) {
		$.rb.features.onDeleteConfirm( ev );
	});
	$( '#rbPostFeatureAddImage' ).click( function( ev ) {
		// Read currently associated image ids.
		// var divArticle = ev.target.closest( '.rb-feature-div' );
		var divEditor = $( '#rbPostFeatureDiv' );
		var strMediaIds = "[" + $( '.rb-post-feature-media-ids', divEditor ).val() + "]";
		var jMediaIds = JSON.parse( strMediaIds );
		$.rb.galleryUpload.open( jMediaIds, 
			$.rb.features.onSubmitEditMediaIds );
	});
});


