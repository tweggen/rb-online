<?php defined('SYSPATH') or die('No direct access allowed.');

class Kohana_Rb {

    /**
    * @var array configuration settings
    */
    protected $_config = array();


    /**
     * Class Main Constructor Method
     * This method is executed every time your module class is instantiated.
     */
    public function __construct() {
        // Loading module configuration file data
        $this->_config = Kohana::$config->load('rb')->as_array();

        // echo 'Hello Modulename! '.$this->_config['some_config_value'];
    }


    protected static $_instance = null;


    /**
     * Return a configuration item.
     */
    public function getConfig( $key ) {
        return $this->_config[$key];
    }


    public static function instance() {
        if( null==self::$_instance ) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

}
