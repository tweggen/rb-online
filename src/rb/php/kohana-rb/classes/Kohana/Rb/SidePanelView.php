<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Rb_SidePanelView extends View
{
	protected $_rbStandardView;

	public function setStandardView( $rbStandardView )
	{
		$this->_rbStandardView = $rbStandardView;
	}

	public function render( $file=NULL ) {
		$strHtml = "";
		$panels = $this->_rbStandardView->getSidepanels();

		foreach( $panels as $panel ) {
			$id = $panel["id"];
			$title = $panel["title"];
			$view = $panel["view"];
			$strHtml .= $id;
//			<<<EOD
//<div class="col-md-8 rb-mainpanel-outer rb-content-fragment" id="rb-content-$id">
//    <div class="rb-mainpanel-inner">
//        <div class="rb-panel">
//EOD;
//			$strHtml .= $view->render();
//			$strHtml .= <<<EOD
//        </div>
//    </div>
//</div>
//EOD;
		}
		return $strHtml;
	}
}