<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Rb_StandardView extends View
{

    static private $_instance = null;

    static public function getInstance() {
        if( null==static::$_instance ) {
            static::$_instance = new Kohana_Rb_StandardView();
        }
        return static::$_instance;
    }
    /**
     * List of top level pages. List items are PageEntries.
     */
    protected $_pages = array();


    /**
     * List of objects in the side panel.
     */
    protected $_sidepanels = array();


    /**
     * Add a new page to this view.
     */
    public function addPage( $id, $strTitle, $view ) {
        $page = array();
        $page["id"] = $id;
        $page["view"] = $view;
        $page["title"] = $strTitle;
        array_push( $this->_pages, $page );
    }


    public function addSidepanel( $id, $strTitle, $view ) {
        $panel = array();
        $panel["id"] = $id;
        $panel["view"] = $view;
        $panel["title"] = $strTitle;
        array_push( $this->_sidepanels, $panel );
    }


    public function getPages() {
        return $this->_pages;
    }


    public function getSidepanels() {
        return $this->_sidepanels;
    }


	public function render( $file=null ) {
		$this->set( "rbStandardView", $this );
		return parent::render( "standard-content" );
	}
}