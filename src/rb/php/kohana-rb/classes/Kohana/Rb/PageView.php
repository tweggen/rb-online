<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Rb_PageView extends View
{
	protected $_rbStandardView;

	public function setStandardView( $rbStandardView )
	{
		$this->_rbStandardView = $rbStandardView;
	}

	public function render( $file=NULL ) {

		$pages = $this->_rbStandardView->getPages();
		$strHtml = "";
		
		foreach( $pages as $page ) {
			$id = $page["id"];
			if( "-" == $id ) {
				continue;
			}
			$title = $page["title"];
			$view = $page["view"];
			$strHtml .= <<<EOD
<div class="col-md-8 rb-mainpanel-outer rb-content-fragment" id="rb-content-$id">
    <!-- div class="rb-mainpanel-inner" -->
        <div class="rb-panel">
EOD;
			$strHtml .= $view->render();
			$strHtml .= <<<EOD
        </div>
    <!-- /div -->
</div>
EOD;

		}
		return $strHtml;
	}
}