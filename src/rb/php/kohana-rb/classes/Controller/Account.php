<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Account extends Controller {

	public function action_login()
	{
		if( HTTP_Request::POST == $this->request->method() ) {
			try {
				// Create the user using form values
				$post = $this->request->post();
				error_log(print_r($post, TRUE)); 
				$success = Auth::instance()->login(
					$post['rbLoginModalUser'], $post['rbLoginModalPassword'] );

				// Reset values so form is not sticky
				$_POST = array();

				if( $success ) {
					error_log( "success is true" );

					// Login successful. Redirect to  homepage, logged in.
					HTTP::redirect('welcome/index');

					return;
				} else {
					error_log( "success is false" );

					// Login failed. For now, also redirect to home page.
					HTTP::redirect('welcome/index');

					return;
				}


			} catch (ORM_Validation_Exception $e) {
				// Set failure message
				$message = 'There were errors, please see form below.';

				// Set errors using custom messages
				$errors = $e->errors('models');
			}
		}
		// Login failed. For now, also redirect to home page.
		HTTP::redirect('welcome/index');
	}


	public function action_logout()
	{
		if (Auth::instance()->logged_in())
		{
			Auth::instance()->logout();
		}
		// Login failed. For now, also redirect to home page.
		HTTP::redirect('welcome/index');
	}


	public function action_createuser()
	{
		$view = View::factory( 'accountcreateuser' )
			->bind( 'message', $message )
			->bind( 'errors', $errors );

		if( HTTP_Request::POST == $this->request->method() ) {
			try {

				// Create the user using form values
				$user = ORM::factory('user')->create_user(
					$this->request->post(), array(
						'username',
						'password',
						'email'
						)
					);

				// Grant user login role
				$user->add('roles', ORM::factory('role', array('name' => 'login') ) );

				// Reset values so form is not sticky
				$_POST = array();

				// Set success message
				$message = "You have added user '{$user->username}' to the database";

			} catch (ORM_Validation_Exception $e) {
				// Set failure message
				$message = 'There were errors, please see form below.';

				// Set errors using custom messages
				$errors = $e->errors('models');
			}
		}
		$this->response->body( $view );
	}

} // End Welcome
