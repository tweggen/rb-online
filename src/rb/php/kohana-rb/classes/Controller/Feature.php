<?php defined('SYSPATH') OR die('No direct script access.');


class Controller_Feature extends Controller_Rest {

/*	public function action_data()
	{

	}*/


	public $indata;


	public function before()
	{
		// Workaround for php builtin server error.
		if( array_key_exists( 'HTTP_CONTENT_TYPE', $_SERVER ) ) {
			$_SERVER['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
		}
		parent::before();
//		if (!$this->request->is_ajax())
//		{
//			$this->request->redirect();
//		}
		$this->indata = file_get_contents( 'php://input' );
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		$this->response->headers( 'Access-Control-Allow-Origin: *' );
	}


	public function action_create()
	{
		$success = false;
		$auth = Auth::instance();
		// error_log( "auth: " . print_r( $auth, true ) );
		$user = $auth->get_user();
		// error_log( "user: " . print_r( $user, true ) );
		$user_id = $user->id;
		// error_log( "user_id: " . print_r( $user_id, true ) );

		error_log( "Called action_create" );
		$post = $this->request->post();
		error_log( print_r( $post, true ) );
		$feature = ORM::factory( 'feature' );
		if( $feature ) {
			$feature->title = $post["title"];
			$feature->user_id = $user_id;
			$feature->creationdate = time();
			$feature->publishdate = time();
			$feature->rendertype = $post["rendertype"];
			$feature->visibility = $post["visibility"];
			$feature->newsdata = $post["newsdata"];

			$feature->save();

			if( array_key_exists( 'media', $post ) ) {
				// diff media, delete or upload
				$oMedia = json_decode( $post['media'] );
				if( $oMedia ) {
					$arrMedia = array();
					foreach( $oMedia as $id ) {
						$media = ORM::factory( 'media', $id );
						if( $media ) {
							array_push( $arrMedia, $media );
						} else {
							$arrMedia = false;
							break;
						}
					}
					if( $arrMedia ) {
						foreach( $arrMedia as $m ) {
							$feature->add( 'media', $m );
						}
					}
				}
				$feature->save();
			}

			$this->rest_output( $feature );
			$success = true;
		} else {
			error_log( "Unable to create feature." );
		}
		if( !$success ) {
			$this->rest_output( array(), 500 );
		} else {
			// Successful: Return feature object.
			$this->rest_output( $feature->toArray(), 200 );
		}
	}


	public function action_update()
	{
		$params = array_merge( $this->request->param(), $this->_params );

		$sucess = false;
		// If an id is given, the corresponding file is returned,
		// otherwise a file list is generated.
		do {
			if( !array_key_exists( 'id', $params ) ) {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Missing key id'),
					'field' => 'id',
				));
			}
			$id = $params['id'];

			// We only can update an existing feature.
			$feature = ORM::factory( 'feature', $id );
			if( !$feature ) {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Unknown feature'),
					'field' => 'id',
				));
			}

			// $this->response->body( $feature->toJson() );
			// TXWTODO: Use a more elaborated update method.
			// Currently, I blindly use title, content and visibility.
			$modified = false;
			if( array_key_exists( 'visibility', $params ) ) { 
				$feature->visibility = $params['visibility'];	
				$modified = true;
			}
			if( array_key_exists( 'title', $params ) ) { 			
				$feature->title = $params['title'];	
				$modified = true;
			}
			if( array_key_exists( 'newsdata', $params ) ) { 
				$feature->newsdata = $params['newsdata'];	
				$modified = true;
			}
			$arrMedia = array();
			if( array_key_exists( 'media', $params ) ) {
				// diff media, delete or upload
				$oMedia = json_decode( $params['media'] );
				if( $oMedia ) {
					foreach( $oMedia as $id ) {
						$media = ORM::factory( 'media', $id );
						if( $media ) {
							array_push( $arrMedia, $media );
						} else {
							$arrMedia = false;
							break;
						}
					}
					if( $arrMedia ) {
						$feature->remove( 'media' );
						foreach( $arrMedia as $m ) {
							$feature->add( 'media', $m );
						}
						$modified = true;
					}
				}
			}

			if( $modified ) {
				$feature->save();
			}

			$success = true;
			$this->rest_output( $feature->toArray() );
		} while(0);

		if( !$success ) {
			$this->rest_output( array(), 500 );
		}
	}


	public function action_index()
	{
		$params = array_merge( $this->request->param(), $this->_params );

		// If an id is given, the corresponding file is returned,
		// otherwise a file list is generated.
		if( array_key_exists( 'id', $params ) ) {
			$id = $params['id'];

			$feature = ORM::factory( 'feature', $id );
			if( $feature ) {

				$this->response->body( $feature->toJson() );

			} else {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Unknown feature'),
					'field' => 'id',
				));

			}
		} else {

			$offset = 0;
			if( array_key_exists( 'offset', $params ) ) {
				$offset = (int) $params['offset'];
			}
			if( $offset < 0 ) $offset = 0;

			$results = array();
			$features = ORM::factory( 'feature' )
				->limit( 20 )->offset( $offset )
				->order_by( 'creationdate', 'DESC' )
				->find_all()
				;

			$offsetindex = $offset;

			foreach( $features as $feature ) {
				$results[$offsetindex++] = $feature->toArray();
			}

			$this->response->body( json_encode(  $results ) );
	
		}
	}


	public function action_delete()
	{
		$params = array_merge( $this->request->param(), $this->_params );

		$sucess = false;
		// If an id is given, the corresponding file is returned,
		// otherwise a file list is generated.
		do {
			if( !array_key_exists( 'id', $params ) ) {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Missing key id'),
					'field' => 'id',
				));
			}
			$id = $params['id'];

			// We only can update an existing feature.
			$feature = ORM::factory( 'feature', $id );
			if( !$feature ) {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Unknown feature'),
					'field' => 'id',
				));
			}

			$feature->visibility = "0";
			$feature->save();

			$success = true;
		} while(0);

		if( !$success ) {
			$this->response->status( 500 );
		} else {
			$this->rest_output(
				array(
					'id' => $id,
				 	'status' => 'deleted',
				 	'oldfeature' => $feature->toArray()
				) );
		}

	}


};

?>