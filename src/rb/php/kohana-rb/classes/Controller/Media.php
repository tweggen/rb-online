<?php defined('SYSPATH') OR die('No direct script access.');


class Controller_Media extends Controller_Rest {

/*	public function action_data()
	{

	}*/


	public $indata;


	public function before()
	{
		// Workaround for php builtin server error.
		if( array_key_exists( 'HTTP_CONTENT_TYPE', $_SERVER ) ) {
			$_SERVER['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
		}
		parent::before();
//		if (!$this->request->is_ajax())
//		{
//			$this->request->redirect();
//		}
		$this->indata = file_get_contents( 'php://input' );
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		$this->response->headers( 'Access-Control-Allow-Origin: *' );
	}


	public function action_create()
	{
		$success = false;
		$auth = Auth::instance();
		// error_log( "auth: " . print_r( $auth, true ) );
		$user = $auth->get_user();
		// error_log( "user: " . print_r( $user, true ) );
		$user_id = $user->id;
		// error_log( "user_id: " . print_r( $user_id, true ) );

		error_log( "Called action_create" );
		$post = $this->request->post();
		error_log( print_r( $post, true ) );

		do {
			if( !array_key_exists( 'files', $post ) ) {
				error_log( "files argument missing." );
				break;
			}
			$arrFiles = $post['files'];
			if( 0==count( $arrFiles ) ) {
				error_log( "no file given in array." );
				break;
			}
			$media = ORM::factory( 'media' );
			if( !$media ) {
				error_log( "Unable to create media." );
				break;
			}
			//error_log( print_r( $media, true ) );
			$media->user_id = $user_id;
			$media->creationdate = time();
			$media->path = $arrFiles[0]['path'];
			$media->mimetype = $arrFiles[0]['mimetype'];
			// $media->title = $arrFiles[0]; 
			$media->save();
			$this->rest_output( $media );
			$success = true;
		} while(0);
		if( !$success ) {
			$this->rest_output( array(), 500 );
		} else {
			// Successful: Return media object.
			$this->rest_output( $media->toArray(), 200 );
		}
	}


	public function action_update()
	{
		$params = array_merge( $this->request->param(), $this->_params );

		$sucess = false;
		// If an id is given, the corresponding file is returned,
		// otherwise a file list is generated.
		do {
			if( !array_key_exists( 'id', $params ) ) {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Missing key id'),
					'field' => 'id',
				));
			}
			$id = $params['id'];

			// We only can update an existing media.
			$media = ORM::factory( 'media', $id );
			if( !$media ) {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Unknown media'),
					'field' => 'id',
				));
			}

			// $this->response->body( $media->toJson() );
			// TXWTODO: Use a more elaborated update method.
			// Currently, I blindly use title, content and visibility.
			$modified = false;
			if( array_key_exists( 'visibility', $params ) ) { 
				$media->visibility = $params['visibility'];	
				$modified = true;
			}
			if( array_key_exists( 'title', $params ) ) { 			
				$media->title = $params['title'];	
				$modified = true;
			}
			if( array_key_exists( 'newsdata', $params ) ) { 
				$media->newsdata = $params['newsdata'];	
				$modified = true;
			}

			if( $modified ) {
				$media->save();
			}

			$success = true;
			$this->rest_output( $media->toArray() );
		} while(0);

		if( !$success ) {
			$this->rest_output( array(), 500 );
		}
	}


	public function action_index()
	{
		$params = array_merge( $this->request->param(), $this->_params );

		// If an id is given, the corresponding file is returned,
		// otherwise a file list is generated.
		if( array_key_exists( 'id', $params ) ) {
			$id = $params['id'];

			$media = ORM::factory( 'media', $id );
			if( $media ) {

				$this->response->body( $media->toJson() );

			} else {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Unknown media'),
					'field' => 'id',
				));

			}
		} else {

			$offset = 0;
			if( array_key_exists( 'offset', $params ) ) {
				$offset = (int) $params['offset'];
			}
			if( $offset < 0 ) $offset = 0;

			$results = array();
			$medias = ORM::factory( 'media' )
				->limit( 300 )->offset( $offset )
				->order_by( 'creationdate', 'ASC' )
				->find_all()
				;

			$offsetindex = $offset;

			foreach( $medias as $media ) {
				$results[$offsetindex++] = $media->toArray();
			}

			$this->response->body( json_encode(  $results ) );
	
		}
	}


	public function action_delete()
	{
		$params = array_merge( $this->request->param(), $this->_params );

		$sucess = false;
		// If an id is given, the corresponding file is returned,
		// otherwise a file list is generated.
		do {
			if( !array_key_exists( 'id', $params ) ) {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Missing key id'),
					'field' => 'id',
				));
			}
			$id = $params['id'];

			// We only can update an existing media.
			$media = ORM::factory( 'media', $id );
			if( !$media ) {
				throw HTTP_Exception::factory(404, array(
					'error' => __('Unknown media'),
					'field' => 'id',
				));
			}

			$media->visibility = "0";
			$media->save();

			$success = true;
		} while(0);

		if( !$success ) {
			$this->response->status( 500 );
		} else {
			$this->rest_output(
				array(
					'id' => $id,
				 	'status' => 'deleted',
				 	'oldmedia' => $media->toArray()
				) );
		}

	}


};

?>