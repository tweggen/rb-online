<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin extends Controller {


	public function action_index()
	{
		$this->response->body( View::factory( 'admin' ) );
	}


	public function action_initdb()
	{
		if( 1 || HTTP_Request::POST == $this->request->method() ) {

			// Currently there are no parameters.
			if( 0 ) {
				// Create the user using form values
				$post = $this->request->post();
				error_log(print_r($post, TRUE)); 
				$success = Auth::instance()->login(
					$post['rbLoginModalUser'], $post['rbLoginModalPassword'] );
			}

			// Reset values so form is not sticky
			$_POST = array();

			$sqlstorage = Model::factory( 'sqlstorage' );
			$sqlstorage->create_database_tables();

			HTTP::redirect('admin/index');

		}
		// No post. Just redirect
		HTTP::redirect('admin/index');
	}


} // End Welcome
