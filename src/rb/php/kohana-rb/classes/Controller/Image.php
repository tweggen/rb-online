<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Image extends Controller {


	private function getOriginal( $filename )
	{
		$path = $_SERVER["DOCUMENT_ROOT"] . PARENTURL . UPLOADDIR . $filename;
		error_log( "Shall read file $filename, path $path." );
		$this->response 
			->headers(
				'content-type',
				File::mime( $path ) )
    		->body(
    			file_get_contents( $path ) );
	}


	/**
	 * Return a scaled version of the given file.
	 * If it does not exist, create it. If $dosave
	 * is set, save it for later use.
	 */ 
	private function getScaled( $filename, $w, $h, $isminsize, $dosave )
	{
		$orgpath = $_SERVER["DOCUMENT_ROOT"] . PARENTURL . UPLOADDIR . $filename;
		$w = (int) $w;
		if( $w < 80 || $w > 1600 
				|| $h < 80 || $h > 1600 ) {
			throw HTTP_Exception::factory(404, __('Invalid size') );			
		}

		$sizedir = "${w}x${h}";

		// TXWTODO: Remove the hard coded size check.
		switch( $sizedir ) {
		case "125x125":
		case "500x500":
		case "1600x1600":
			break;
		default:
			throw HTTP_Exception::factory(404, __('Unsupported size') );			
		}

		$dirpath =  $_SERVER["DOCUMENT_ROOT"] . PARENTURL . UPLOADDIR . 
			$sizedir;

		// Already scaled?
		$imgpath = $dirpath . "/" . $filename;
		if( file_exists( $imgpath ) ) {
			error_log( "Cache hit for $filename" );
			$this->response 
				->headers(
					'content-type',
					File::mime( $imgpath ) )
	    		->body(
	    			file_get_contents( $imgpath ) );
	    	return;
		}

		// Does not exist, so create the scaled version.
		if( !file_exists( $dirpath ) ) {
 		   mkdir( $dirpath, 0777, true );
		}
		$img = Image::factory( $orgpath );
		if( !$img ) {
			throw HTTP_Exception::factory(404, __('Unable to load image') );			
		}
		$img->resize( $w, $h, $isminsize?Image::INVERSE:Image::AUTO );
		$img->save( $imgpath, 80 /* quality */ );

		if( file_exists( $imgpath ) ) {
			$this->response 
				->headers(
					'content-type',
					File::mime( $imgpath ) )
	    		->body(
	    			file_get_contents( $imgpath ) );
			error_log( "Cache miss for $filename" );
		} else {
			// Unable to create image, return original.
			$this->response 
				->headers(
					'content-type',
					File::mime( $imgpath ) )
	    		->body(
	    			file_get_contents( $orgpath ) );
			error_log( "Cache error for $filename" );
		}
	}

	/**
	 * Return a properly sized image.
	 */
	public function action_get()
	{
		if( HTTP_Request::GET != $this->request->method() ) {
			throw HTTP_Exception::factory( 404, __('Unsupported method') );
		}

		$w = 125; 
		$h = 125;

		// We do not want to cache all sizes.
		// Detail view might be scaled in real time.
		$dosave = false;
		$doorg = false;
		$isminsize = false;

		// TXWTODO: Test, wether the queries exist.

		// Strip off path to prevent problems.
		$filename = $this->request->query( 'n', NULL );
		if( !$filename ) {
			throw HTTP_Exception::factory(404, array(
				'error' => __('Unknown image'),
				'field' => 'n',
			));
		}
		$filename = basename( $filename );
		if( !$filename || ""==$filename ) {
			throw HTTP_Exception::factory(404, array(
				'error' => __('Empty filename'),
				'field' => 'n',
			));
		}
		$size = $this->request->query( 's', NULL );

		switch( $size ) {
			// Thumbnails View
		case 't':
			$w = 125;
			$h = 125;
			$dosave = true;
			$isminsize = true;
			break;

			// Feed View
		default:
		case 'f':
			$w = 500;
			$h = 500;
			$dosave = true;
			$isminsize = true;
			break;

			// Single View
		case 's':
			$w = 1600;
			$h = 1600;
			$isminsize = false;
			break;

		case 'org':
			$doorg = true;
			break;

		}

		/** 
		 * If we ought to return the original, we do not need to scale
		 * anything.
		 */ 
		if( $doorg ) {
			return $this->getOriginal( $filename );
		} else {
			return $this->getScaled( $filename, $w, $h, $isminsize, $dosave );
		}
	}

	public function action_index()
	{
		// $this->response->body( View::factory( 'admin' ) );
		HTTP::redirect('index');
	}


	public function action_initdb()
	{
		if( 1 || HTTP_Request::POST == $this->request->method() ) {

			// Currently there are no parameters.
			if( 0 ) {
				// Create the user using form values
				$post = $this->request->post();
				error_log(print_r($post, TRUE)); 
				$success = Auth::instance()->login(
					$post['rbLoginModalUser'], $post['rbLoginModalPassword'] );
			}

			// Reset values so form is not sticky
			$_POST = array();

			$sqlstorage = Model::factory( 'sqlstorage' );
			$sqlstorage->create_database_tables();

			HTTP::redirect('admin/index');

		}
		// No post. Just redirect
		HTTP::redirect('admin/index');
	}


} // End Welcome
