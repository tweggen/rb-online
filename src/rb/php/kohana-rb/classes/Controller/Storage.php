<?php defined('SYSPATH') or die('No direct script access.');




class Controller_Storage extends Controller_Rest {

	public static $storagePath = "../../docs/fatmet/cyfm/";

	public $indata;

	protected function findStorageDir()
	{
		if( !file_exists( self::$storagePath ) ) {
			mkdir( self::$storagePath, 0777, true );
		}
		return true;
	}

	public function before()
	{
		// Workaround for php builtin server error.
		if( array_key_exists( 'HTTP_CONTENT_TYPE', $_SERVER ) ) {
			$_SERVER['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
		}
		parent::before();
//		if (!$this->request->is_ajax())
//		{
//			$this->request->redirect();
//		}
		$this->indata = file_get_contents( 'php://input' );
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		$this->response->headers( 'Access-Control-Allow-Origin: *' );
	}


	public function action_data()
	{
		$this->response->body(json_encode(array('test'=>'json')));
	}


	public function action_update()
	{
		// error_log( print_r( $this, true ) );
		$params = array_merge( $this->request->param(), $this->_params );
		if( !array_key_exists( 'id', $params ) ) {
			// Id is a required parameter for the update call.
			throw HTTP_Exception::factory(400, array(
				'error' => __('Missing id'),
				'field' => 'id',
			));
		}
		if( !array_key_exists( 'doc', $params ) ) {
			// Id is a required parameter for the update call.
			throw HTTP_Exception::factory(400, array(
				'error' => __('Missing doc'),
				'field' => 'doc',
			));
		}

		$id = $params['id'];
		$doc = $params['doc'];
		$path = self::$storagePath . "currfm-$id.json";
		$this->findStorageDir();
		file_put_contents( $path, json_encode( $doc ) );
		$this->response->body( json_encode( array( 'result' => 'ok' ) ) );
	}


	public function action_index()
	{
		// error_log( print_r( $this, true ) );
		$params = array_merge( $this->request->param(), $this->_params );

		// If an id is given, the corresponding file is returned,
		// otherwise a file list is generated.
		if( array_key_exists( 'id', $params ) ) {
			$id = $params['id'];
			$path = self::$storagePath . "currfm-$id.json";
			$this->findStorageDir();
			$data = file_get_contents( $path );
			$this->response->body( $data );
		} else {
			$this->response->body( json_encode( array( 'result' => 'nyi', 'cwd' => getcwd() ) ) );
		}
	}

} // End Storage
