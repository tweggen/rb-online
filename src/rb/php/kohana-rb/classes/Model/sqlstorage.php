<?php defined('SYSPATH') OR die('No Direct Script Access');

Class Model_Sqlstorage extends Model {

	public function create_user_tables() {
		DB::query( null, <<<EOT
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
EOT
)->execute();
		DB::query( null, <<<EOT
INSERT INTO `roles` (`id`, `name`, `description`) VALUES(1, 'login', 'Login privileges, granted after account confirmation');
EOT
)->execute();
		DB::query( null, <<<EOT
INSERT INTO `roles` (`id`, `name`, `description`) VALUES(2, 'admin', 'Administrative user, has access to everything.');
EOT
)->execute();
		DB::query( null, <<<EOT
CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY  (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOT
)->execute();
		DB::query( null, <<<EOT
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `logins` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_login` int(10) UNSIGNED,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
EOT
)->execute();
		DB::query( null, <<<EOT
CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  `expires` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
EOT
)->execute();
		DB::query( null, <<<EOT
ALTER TABLE `roles_users`
  ADD CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
EOT
)->execute();
		DB::query( null, <<<EOT
ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
EOT
)->execute();
	}

	public function create_media_tables() {
		DB::query( null, <<<EOT
CREATE TABLE IF NOT EXISTS `media` (
 `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
 `path` varchar(255) NOT NULL,
 `creationdate` int(10) UNSIGNED NOT NULL,
 `user_id` int(10) UNSIGNED NOT NULL,
 `mimetype` varchar(48) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_user_id` (`user_id`),
 KEY `creationdate` (`creationdate`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOT
)->execute();
		DB::query( null, <<<EOT
ALTER TABLE `media`
 ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
EOT
)->execute();
	}

	public function create_news_tables() {
    DB::query( null, <<<EOT
CREATE TABLE IF NOT EXISTS `media_news` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `feature_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY  (`media_id`,`feature_id`),
  KEY `fk_media_id` (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOT
)->execute();
		DB::query( null, <<<EOT
CREATE TABLE IF NOT EXISTS `news` (
 `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
 `title` varchar(80) NOT NULL,
 `user_id` int(10) UNSIGNED NOT NULL,
 `creationdate` int(10) UNSIGNED NOT NULL,
 `publishdate` int(10) UNSIGNED,
 `rendertype` smallint UNSIGNED,
 `visibility` smallint UNSIGNED,
 `newsdata` TEXT NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_user_id` (`user_id`),
 KEY `creationdate` (`creationdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOT
)->execute();
		DB::query( null, <<<EOT
ALTER TABLE `news`
 ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
EOT
)->execute();
	}

	public function create_content_tables() {
		$this->create_media_tables();
		$this->create_news_tables();
	}

	public function create_database_tables() {
		$this->create_user_tables();
		$this->create_content_tables();
	}
}

?>