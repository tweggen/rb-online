<?php defined('SYSPATH') OR die('No Direct Script Access');

class Model_Feature extends ORM {
 
	protected $_belongs_to = array(
	    'user' => array(
	        'model'       => 'User',
	        'foreign_key' => 'user_id',
	    ),
	);

	protected $_has_many = array(
		'media' => array(
			'model' => 'media',
			'through' => 'media_news'
		),
	);

	protected $_table_name = 'news';

    public function rules() {
    			//array(array($this,'unique'),array('bla',':value')),

    	return array(
    		'title' => array(
    			array('not_empty'),
    			array('max_length', array(':value',80)),
    		),
    		'creationdate' => array(
    			array('not_empty'),
    		),
    		'newsdata' => array(
    			array('not_empty'),
    		),
    	);
    }
/*
	public function labels()
	{
		return array(
			'id'         	=> 'id',
			'title' 		=> 'title',
			'creationdate'  => 'creation_date',
			'newsdata'		=> 'news_data'
		);
	}*/

	public function toArray() {
		$allmedia = $this->media->find_all();
		$arrMedia = array();
		foreach( $allmedia as $onemedia ) {
			error_log( $onemedia->id );
			array_push( $arrMedia, $onemedia->toArray() );
		}
		return array(
			'id' => $this->id,
			'title' => $this->title,
			'author_id' => $this->user_id,
			'author' => $this->user->username,
			'creationdate' => $this->creationdate,
			'publishdate' => $this->publishdate,
			'rendertype' => $this->rendertype,
			'visibility' => $this->visibility,
			'newsdata' => $this->newsdata,
			'media' => $arrMedia
		);
	}

	public function toJson() {
		return json_encode( $this->toArray() );
	}
}

?>