<?php defined('SYSPATH') OR die('No Direct Script Access');


class Model_Media extends ORM {
 
	protected $_belongs_to = array(
	    'user' => array(
	        'model'       => 'User',
	        'foreign_key' => 'user_id',
	    ),
	);

	// Relationships
	protected $_has_many = array(
		'news' => array(
			'model' => 'feature',
			'through' => 'media_news'
		),
	);


	protected $_table_name = 'media';

    public function rules() {
    	return array(
    		'mimetype' => array(
    			array('not_empty'),
    			array('max_length', array(':value',48)),
    		),
    		'creationdate' => array(
    			array('not_empty'),
    		),
    		'path' => array(
    			array('not_empty'),
    			array('max_length', array(':value',255)),
    		),
    	);
    }

/*
	public function labels()
	{
		return array(
			'id'         	=> 'id',
			'title' 		=> 'title',
			'creationdate'  => 'creation_date',
			'newsdata'		=> 'news_data'
		);
	}*/


	public function toArray() {
		return array(
			'id' => $this->id,
			'path' => $this->path,
			'author_id' => $this->user_id,
			'author' => $this->user->username,
			'creationdate' => $this->creationdate,
			'mimetype' => $this->mimetype
		);
	}


	public function toJson() {
		return json_encode( $this->toArray() );
	}
}

?>