<?php defined('SYSPATH') OR die('No Direct Script Access'); ?>


		<div id="links" class="links" />
		</div>


<!-- div for the gallery, invisible in the beginning -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

        <!-- Gallery model -->
        <div class="modal fade" id="rbGalleryUploadModal" tabindex="-1" role="dialog" aria-labelledby="rbGalleryUploadModalLabel">
        	<div class="modal-dialog" role="document">
        		<div class="modal-content">
        			<form action="#" id="rbGalleryUploadModalForm" method="post">
        				<div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="rbGalleryUploadModalLabel">Bild ausw&auml;hlen</h4>
                        </div>
                        <div class="modal-body">
                        	<div id="rbGalleryUploadModalGrid">
                        	</div>
                        	<input id="rbGalleryUploadFileUploadHiddenName" type="hidden" name="hiddenname"> 

						    <!-- The global progress bar -->
						    <div id="rbGalleryUploadFileUploadProgress" class="progress">
						        <div class="progress-bar progress-bar-success"></div>
						    </div>
						    <!-- The container for the uploaded files -->
						    <div id="rbGalleryUploadFileUploadFiles" class="files"></div>
                    	</div>
                    	<div class="modal-footer">
                    		<button id="rbGalleryUploadDoUpload" type="button" class="btn fileinput-button">
								<span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Bild hochladen...
<input 
	id="rbGalleryUploadFileUpload" 
	type="file" 
	name="files[]" 
	data-url="<?php echo BASEURL . 'php/upload/'; ?>" 
	>
<!-- multiple commented out for this fversion --> 
							</button>
	                        <button type="submit" class="btn btn-default">Ausw&auml;hlen</button>
                    	</div>
        			</form>
        		</div>
        	</div>
        </div>

	    <script type="text/javascript" src="<?php echo BASEURL.Rb::instance()->getConfig( "site_name" ).".min.js"; ?>"></script>


        <!-- Login modal -->
        <div class="rb-invis">
		</div>

    </body>

</html>