<?php defined('SYSPATH') OR die('No Direct Script Access');

$rb = Rb::instance();

$rbTitle = $rb->getConfig( "main_title" ) . " - Administration";

$Auth = Auth::instance();

$AuthLoggedIn = false;
if( $rb->getConfig( "first_install" ) ) {
    $AuthLoggedIn = true;
} else {
    $AuthLoggedIn = $Auth->logged_in();
}

if( $AuthLoggedIn ) {
    // Required.
} else {
    // Not allowed.
	die( 'Requires authentication.' );
}

include 'head.php';

?>
<div class="rb-content">
    <div class="rb-content-wrapper">
        <div>
            <div class="row">
                <div class="col-md-12 rb-mainpanel-outer">
                    <div class="rb-mainpanel-inner">
                    	<h4>Create User</h4>
                    	<p>
                            <a class="btn btn-default" href="<?php echo URL::site('account/createuser') ?>" role="button">Create User</a>
                            New users can be created by the administrator, roles can be assigned.
                        </p>
                        <h4>(Re-)initialize Database</h2>
                        <p>
                            <a class="btn btn-default" href="<?php echo URL::site('admin/initdb') ?>" role="button">Init DB</a>
                            After initialization or update, ensure that proper database tables are implemented.
                        </p>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- rb-content-home -->
    </div> <!-- end of main content div class "rb-content-wrapper" -->
<div> <!-- end of rb-content -->

<?php
include 'tail.php';

?>