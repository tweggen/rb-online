<?php defined('SYSPATH') OR die('No Direct Script Access');

$rb = Rb::Instance();
$rbTitle = $rb->getConfig( "main_title" ) . " home page";

global $Auth;
$Auth = Auth::instance();

global $AuthLoggedIn;
$AuthLoggedIn = false;
if( $rb->getConfig( "first_install" ) ) {
    $AuthLoggedIn = true;
	$UserName = "FirstInstall";
} else {
    $AuthLoggedIn = $Auth->logged_in();
	if( $AuthLoggedIn ) {
		$User = $Auth->get_user();
		$UserName = $User->username;
	}
}

if( !$AuthLoggedIn ) {
	$UserName = "Nobody";
}

//include 'layout.php';

include 'head.php';
include 'navbar.php';
include 'maincontent.php';
include 'tail.php';

?>
