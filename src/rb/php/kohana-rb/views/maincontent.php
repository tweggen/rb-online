<?php defined('SYSPATH') OR die('No Direct Script Access'); 

?>

<!-- content fragments, each named with id rb-content-xxx -->
<div class="rb-content">
    <div class="rb-content-wrapper">
        <div>
            <div class="row">
<?php

$rbPageView = new Rb_PageView();
$rbPageView->setStandardView( $rbStandardView );
echo $rbPageView->render();

// include 'content-features.php';
// include 'content-media.php';
// include 'content-gallery.php';
// include 'content-calendar.php';
// include 'content-contact.php';
// include 'content-imprint.php';
?>
                <div class="col-md-4 rb-sidepanel-outer">
                  	<div class="rb-sidepanel-inner">
<?php

$rbSidepanelView = new Rb_SidePanelView();
$rbSidepanelView->setStandardView( $rbStandardView );
echo $rbSidepanelView->render();
?>
                    	<div class="rb-panel">
                        	<h4>News</h4>
                        	<div id="rbFeatureHeadlineContainer"></div>
                    	</div>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- rb-content-home -->
    </div> <!-- end of main content div class "rb-content-wrapper" -->
<div> <!-- end of rb-content -->

