<?php defined('SYSPATH') OR die('No Direct Script Access');

$rb = Rb::instance();

$rbTitle = $rb->getConfig( "main_title" ) . " - Create User";

$Auth = Auth::instance();

$AuthLoggedIn = $Auth->logged_in();

if( $AuthLoggedIn ) {
	fail( "Already logged in." );
} else {
	$UserName = "Nobody";
}

include 'head.php';

?>
<div class="rb-content">
    <div class="rb-content-wrapper">
        <div>
            <div class="row">
                <div class="col-md-8 rb-mainpanel-outer">
                    <div class="rb-mainpanel-inner">
                    	<h4>Create User
                    	</h4>
                    	<p></p>
<h2>Create a New User</h2>
<? if ($message) : ?>
<h3 class="message">
<?= $message; ?>
</h3>
<? endif; ?>

<?= Form::open( 'user/create',
        array( 'class' => 'form-horizontal' )
); ?>

<div class='form-group'>
    <div class='col-sm-4'>
        <?= Form::label('username', 'Username'); ?>
    </div>
    <div class='col-sm-8'>
        <?= Form::input( 'username',
            HTML::chars(Arr::get($_POST, 'username')),
            array(
                'placeholder' => 'z.B. Fritz Haarmann',
                'class' => 'form-control'
            )
        ); ?>
        <div class="error">
            <?= Arr::get($errors, 'username'); ?>
        </div>
    </div>
</div>

<div class='form-group'>
    <div class='col-sm-4'>
        <?= Form::label('email', 'Email Address'); ?>
    </div>
    <div class='col-sm-8'>
        <?= Form::input('email', HTML::chars(Arr::get($_POST, 'email')),
            array(
                'placeholder' => 'z.B. fritz@96.de',
                'class' => 'form-control'
            )
        ); ?>
        <div class="error">
            <?= Arr::get($errors, 'email'); ?>
        </div>
    </div>
</div>

<div class='form-group'>
    <div class='col-sm-4'>
        <?= Form::label('password', 'Password'); ?>
    </div>
    <div class='col-sm-8'>
        <?= Form::password('password', NULL,
            array(
                'placeholder' => 'Passwort',
                'class' => 'form-control'
            )
        ); ?>
        <div class="error">
            <?= Arr::path($errors, '_external.password'); ?>
        </div>
    </div>
</div>

<div class='form-group'>
    <div class='col-sm-4'>
        <?= Form::label('password_confirm', 'Confirm Password'); ?>
    </div>
    <div class='col-sm-8'>
        <?= Form::password('password_confirm', NULL,
            array(
                'placeholder' => 'Passwort wiederholen',
                'class' => 'form-control'
            )
        ); ?>
        <div class="error">
            <?= Arr::path($errors, '_external.password_confirm'); ?>
        </div>
    </div>
</div>

<?= Form::submit( 'create', 'Create User',
    array(
        'class' => 'btn btn-default',
        'type' => 'submit'
    )
); ?>
<?= Form::close(); ?>

<!--
                		<form action="<?= BASEURL."/account/createuser"; ?>" method="post" accept-charset="utf-8">
                			<div class="form-group">
                				<label for="username">
                                    Benutzername
                                </label>
	                			<input id="username" type="text" class="form-control" placeholder="z.B. Fritz Mustermann">
                                <label for="username" class="error">
                                    <?= Arr::get($errors, 'username'); ?>
                                </label>
                            </div>
                			<div class="form-group">
                				<label for="password">
                                    Passwort
                                </label>
	                			<input id="password" type="password" class="form-control">
                                <label for="password" class="error">
                                    <?= Arr::get($errors, 'password'); ?>
                                </label>
	                		</div>
                			<div class="form-group">
                				<label for="password_confirm">
                                    Passwort (wiederholen)
                                </label>
	                			<input id="password_confirm" type="password" class="form-control">
                                <label for="password_confirm" class="error">
                                    <?= Arr::path($errors, '_external.password_confirm'); ?>	                		
                                </label>
                            </div>
                			<div class="form-group">
                				<label for="newemail">eMail Adresse</label>
	                			<input id="newemail" type="text" class="form-control">
	                		</div>
	                		<button type="submit">Anlegen!</button>
                		</form>
                    -->
                    </div>
                </div>
                <div class="col-md-4 rb-sidepanel-outer">
                  	<div class="rb-sidepanel-inner">
                    	<div class="rb-panel">
                        	<h4>Warum anmelden</h4>
                        	<p>Weil ich zur Band geh&ouml;re!</p>
                        	<p>Weil ich ein Mega-Fan bin!</p>
                        	<p>Weil ich auf dem laufenden bleiben will!</p>
                    	</div>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- rb-content-home -->
    </div> <!-- end of main content div class "rb-content-wrapper" -->
<div> <!-- end of rb-content -->

<?php
include 'tail.php';

?>