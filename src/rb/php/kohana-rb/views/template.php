<?php defined('SYSPATH') OR die('No Direct Script Access');

$rbTitle = Rb::instance()->getConfig( "main_title" ) . " - Create User";

$Auth = Auth::instance();

$AuthLoggedIn = $Auth->logged_in();

if( $AuthLoggedIn ) {
	fail( "Already logged in." );
} else {
	$UserName = "Nobody";
}

include 'head.php';

?>
<div class="rb-content">
    <div class="rb-content-wrapper">
        <div>
            <div class="row">
                <div class="col-md-8 rb-mainpanel-outer">
                    <div class="rb-mainpanel-inner">
<?= $content; ?>
                    </div>
                </div>
                <div class="col-md-4 rb-sidepanel-outer">
                  	<div class="rb-sidepanel-inner">
                    	<div class="rb-panel">
                        	<h4>Warum anmelden</h4>
                        	<p>Weil ich zur Band geh&ouml;re!</p>
                        	<p>Weil ich ein Mega-Fan bin!</p>
                        	<p>Weil ich auf dem laufenden bleiben will!</p>
                    	</div>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- rb-content-home -->
    </div> <!-- end of main content div class "rb-content-wrapper" -->
<div> <!-- end of rb-content -->

<?php
include 'tail.php';

?>