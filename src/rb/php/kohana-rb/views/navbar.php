<?php defined('SYSPATH') OR die('No Direct Script Access'); ?>
        <!-- Navigation bar -->
		<nav class="navbar navbar-default"> 
            <!-- TXWTODO: On mobile devices, the navbar should be navbar-fixed-top -->
            <div id="rb-containerfluid" class="container-fluid">
                <div class="navbar-header"> <!--  collapsed aria-expanded="false" aria-controls="navbar"-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- a class="navbar-brand" href="#">TFF</a -->
                </div>

                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav"><?php
    //$rbStandardView = Kohana_Rb_StandardView::getInstance();
    $pages = $rbStandardView->getPages();
    $strFirst = " rb-page-active";
    foreach( $pages as $page ) {
        $title = $page["title"];
        $id = $page["id"];
        $strHtml = "";
        if( "-" == $id ) {
            // Advance to the right side
            $strHtml = <<<EOD
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
EOD;
        } else {
            $strHtml = <<<EOD
                        <li>
                            <a href="#" class="rb-page-button$strFirst" data-rbpage="rb-content-$id">$title</a>
                        </li>        
EOD;
            $strFirst = "";
        }
        echo $strHtml;
    }

// Display Logout/Configure if logged in
// Display Menu Login / Create Account if not logged in
// For this site, just a small icon
if( $AuthLoggedIn ) {
	?><li class="menu-item dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">&nbsp;<span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;<?php echo $UserName; ?></a>
		<ul class="dropdown-menu">
			<li class="menu-item">
            	<a href="<?php echo URL::site('account/settings') ?>">Einstellungen...</a>
        	</li>
            <li class="menu-item">
                <a href="<?php echo URL::site('admin/index') ?>">Admin...</a>
            </li>
			<li class="menu-item">
            	<a href="<?php echo URL::site('account/logout') ?>">Abmelden</a>
        	</li>
            <li class="menu-item">
                <a href="<?php echo URL::site('account/createuser') ?>">Neuer Nutzer...</a>
            </li>
    	</ul>
	</li><?php
} else {
	?><li class="menu-item dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">&nbsp;<span class="glyphicon glyphicon-user" aria-hidden="true"></a>
		<ul class="dropdown-menu">
			<li class="menu-item">
            	<a href="#" data-toggle="modal" data-target="#rbLoginModal">Anmelden...</a>
        	</li>
    	</ul>
	</li><?php
}

?></ul>
                </div>

                <div class="modal fade" id="rbLoginModal" tabindex="-1" role="dialog" aria-labelledby="rbLoginModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="<?php echo URL::site('account/login') ?>" id="rbLoginModalForm" method="post">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="rbLoginModalLabel">Anmelden</h4>
                          </div>
                          <div class="modal-body">
                              <div class="form-group">
                                <label for="rbLoginModalUser">Benutzername</label>
                                <input type="text" class="form-control" name="rbLoginModalUser" id="rbLoginModalUser" placeholder="john_doe">
                              </div>
                              <div class="form-group">
                                <label for="rbLoginModalPassword">Password</label>
                                <input type="password" class="form-control" name="rbLoginModalPassword" id="rbLoginModalPassword" placeholder="Password">
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn btn-default">Anmelden</button>
                            <!-- button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Anmelden</button -->
                          </div>
                        </form>

                    </div>
                  </div>
                </div>


            </div>
        </nav>
        <!-- end of navigation bar -->
