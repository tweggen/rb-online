<?php defined('SYSPATH') OR die('No Direct Script Access'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
        <!-- The order of the firsst three meta tags is relevant! -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- TODO: Enter description here -->
        <meta name="description" content="<?php echo Rb::instance()->getConfig( 'main_title' ); ?> homepage">
        <!-- TODO: Enter author here -->
        <meta name="author" content="Timo Weggen">	
        <!-- Our compressed style sheet -->
    	<link href="<?php echo BASEURL.Rb::instance()->getConfig( "site_name" ).".css"; ?>" rel="stylesheet">

        <script>
var RbDataURL = "<?php echo URL::site('data/docs'); ?>";
var RbHomeURL = "<?php echo URL::site('welcome/index'); ?>";
var RbBaseURL = "<?php echo BASEURL; ?>";
var RbParentURL = "<?php echo PARENTURL; ?>";
var RbLoggedIn = "<?php echo $AuthLoggedIn; ?>";
var RbFilesRoot = '<?php echo PARENTURL . UPLOADDIR; ?>';
        </script>

        <!-- Title as displayed on top of the page, HTML escaped characters -->
        <title><?php echo $rbTitle; ?></title>

        <!-- manually overridden styles -->
        <!-- Keep these as small as possible, better create a custom stlye file -->
        <style> 
        </style>
    </head>


	<body id="rbbody">
      <!-- Container for everything that is centered -->
      <a href="<?php echo BASEURL."index.php"; ?>"><img class="rb-top-logo"></a>

