<?php defined('SYSPATH') OR die('No Direct Script Access'); ?>

<?php

global $AuthLoggedIn;
// If logged in, show editor bar.
if( $AuthLoggedIn ) {
	?>
	<div class='rb-mainpanel-inner'>
		<div class='rb-panel'>
			<button id="rbShowPostFeature" class="btn btn-default" type="button">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				New article
			</button>
		</div>
	</div>
	<div id="rbPostFeatureParent"></div>
	<div class='collapse rb-mainpanel-inner' id="rbPostFeatureDiv">
		<div class='rb-panel'>
			<p></p>
			<h4 id="rbPostEditTitle">Das solltest Du nicht sehen.</h4>
			<?= Form::open( '#',
				array(
					'class' => 'form-horizontal',
					'id' => 'rbPostFeatureForm'
				)
			); ?>
			<div class='form-group'>
				<div class='col-sm-8'>
					<?= Form::hidden( 'rbPostFeatureMediaIds',
						NULL,
						array(
							'class' => 'rb-post-feature-media-ids'
						)
					 ); ?>
					<?= Form::input( 'rbPostFeatureTitle', 
						NULL,
						array( 
							'placeholder' => 'Titel des Artikels',
							'class' => 'rb-feature-input form-control rb-no-enter-submit'
						)
					); ?>
				</div>
				<div class='col-sm-4'>
					<?= Form::select( 'rbPostFeatureVisibility',
						array(
							'hidden' => 'Versteckt',
							'editors' => 'Redakteure',
							'members' => 'Mitglieder',
							'public' => '&Ouml;ffentlich'
							),
						'editors',
						array(
							'class' => 'rb-feature-input form-control'
						)
					); ?>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-sm-12'>
					<?= Form::textarea( 'rbPostFeatureContent',
					 	NULL,
					 	array(
					 		'placeholder' => 'Artikeltext',
					 		'class' => 'rb-feature-input form-control',
					 		'rows' => '10'
					 	)
					); ?>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-sm-12'>
					<?= Form::button( 'rbPostFeaturePublish', 
						'Ver&ouml;ffentlichen',
						array(
							'id' => 'rbPostFeaturePublish',
							'class' => 'btn btn-default',
							'type' => 'submit'
						)
					); ?>
					<?= Form::button( 'rbPostFeatureDiscard', 
						'Verwerfen',
						array(
							'class' => 'btn',
							'type' => 'reset'
						)
					); ?>
					<button id="rbPostFeatureAddImage" type="button" class="btn pull-right">
						<span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Bild hinzuf&uuml;gen...
					</button>
				</div>
			</div>
			<?= Form::close(); ?>
		</div>
	</div>
	<?php
}
?>
	<div id="rbFeaturesDiv">
		<span id="rbFeaturesHeadDummy"></span>
	</div>

